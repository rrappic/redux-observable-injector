import set from 'lodash/set';
import has from 'lodash/has';
import {ActionsObservable, combineEpics, createEpicMiddleware } from 'redux-observable';
import {Observable} from "rxjs/Observable";
import {of as ObservableOf} from "rxjs/observable/of";
import {filter} from "rxjs/operators/filter";
import {map} from "rxjs/operators/map";
import {withLatestFrom} from "rxjs/operators/withLatestFrom";
import {switchMap} from "rxjs/operators/switchMap";
import {mergeMap} from "rxjs/operators/mergeMap";

let epicMiddleware = {};
const combine = combineEpics;
let rootEpicWrapperFunc = (rootEpic) => rootEpic;
let store = null;

function getPauseActionType(actionKey){
    return "@@PauseableEpic/["+actionKey+"]::PAUSE";
}
function getResumeActionType(actionKey){
    return "@@PauseableEpic/["+actionKey+"]::RESUME";
}
function createPauseAction(actionKey){
    return {type:getPauseActionType(actionKey)};
}
function createResumeAction(actionKey){
    return {type:getResumeActionType(actionKey)};
}

function wrapAsPausableEpic(epic, key){
    epic = (function() {
        const innerEpic = epic;
        const p_r_ActionKey = key || epic.prototype.constructor.name;
        return function() {
            const pauseActionType = getPauseActionType(p_r_ActionKey);
            const resumeActionType = getResumeActionType(p_r_ActionKey);

            return ((action$, store) => {
                    const pauser = action$.pipe(filter(act => 
                        act.type ===  pauseActionType || 
                        act.type === resumeActionType
                    ),switchMap(
                        act => act.type === pauseActionType ? 
                        ObservableOf(true) : 
                        ObservableOf(false)
                    ),map(v => v));

                    return action$.pipe(withLatestFrom(pauser),filter(([action, paused]) => !paused),map(v => v[0])
                            ,filter(act => 
                                act.type !==  pauseActionType &&
                                act.type !== resumeActionType
                            ),mergeMap(action => innerEpic(new ActionsObservable(ObservableOf(action)), store)));
                }
            ).apply(this, arguments);

        };
    })();
    return epic;
}

function wrapperFunctionPausableEpic(epic){
    return wrapAsPausableEpic(epic);
}
function noWrap(epic){
    return epic;
}
function reducerCombineEpics(combinedEpics){
    return combine(...Object.values(combinedEpics));
}
function noReducer(combinedEpics){
    return combinedEpics;
}

function travelEpicsRecursively(epics, wrapFunction, returningReducerFunction){
    // If this is a leaf or already combined.
    if (typeof epics === 'function') {
        return wrapFunction(epics);
    }

    // If this is an object of functions, combine epics.
    if (typeof epics === 'object') {
        let combinedEpics = {};
        for (let key of Object.keys(epics)) {
            combinedEpics[key] = travelEpicsRecursively(epics[key], wrapFunction, returningReducerFunction);
        }
        return returningReducerFunction(combinedEpics);
    }

    // If we get here we have an invalid item in the epic path.
    throw new Error({
        message: 'Invalid item in epic tree',
        item: epics
    });
}

export function createEpicInjectorMiddleware(initialEpics, refEpicSubject = (input$) => {}) {

    const initialEpics_AsPausables = travelEpicsRecursively(initialEpics, wrapperFunctionPausableEpic, noReducer);

    epicMiddleware = createEpicMiddleware(
        travelEpicsRecursively(initialEpics_AsPausables, noWrap, reducerCombineEpics),
        refEpicSubject
    );

    epicMiddleware.injectedEpics = initialEpics_AsPausables;
    epicMiddleware.runningEpicsMap = {};
    Object.keys(initialEpics).forEach(k => {
        epicMiddleware.runningEpicsMap[k] = true;
    });

    const pre_epicMiddleware = (function(){
        const _epicMiddleware = epicMiddleware;
        return function(_store){
            store = _store;
            return _epicMiddleware.apply(this, arguments);
        }
    })(); 

    return pre_epicMiddleware;
}

export function injectEpic(key, epic, force = false, wait = false) {
    // If already set, do nothing.
    if (!has(epicMiddleware.injectedEpics, key) || force){
        set(epicMiddleware.injectedEpics, key, wrapAsPausableEpic(epic, key));
        set(epicMiddleware.runningEpicsMap, key, true);
        
        if(!wait){
            epicMiddleware.replaceEpic(travelEpicsRecursively(epicMiddleware.injectedEpics, noWrap, reducerCombineEpics));
        }
    }
    if(!wait){
        Object.keys(epicMiddleware.injectedEpics).forEach(k => {
            if(has(epicMiddleware.runningEpicsMap, k) && epicMiddleware.runningEpicsMap[k] === true){
                resumeEpic(k)
            }
        });
    }
}

export function pauseEpic(key){
    if(has(epicMiddleware.injectedEpics, key)){
        set(epicMiddleware.runningEpicsMap, key, false);
        store.dispatch(createPauseAction(key));
    }
}

export function resumeEpic(key){
    if(has(epicMiddleware.injectedEpics, key)){
        set(epicMiddleware.runningEpicsMap, key, true);
        store.dispatch(createResumeAction(key));
    }
}