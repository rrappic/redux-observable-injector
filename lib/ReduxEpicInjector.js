'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.createEpicInjectorMiddleware = createEpicInjectorMiddleware;
exports.injectEpic = injectEpic;
exports.pauseEpic = pauseEpic;
exports.resumeEpic = resumeEpic;

var _set = require('lodash/set');

var _set2 = _interopRequireDefault(_set);

var _has = require('lodash/has');

var _has2 = _interopRequireDefault(_has);

var _reduxObservable = require('redux-observable');

var _Observable = require('rxjs/Observable');

var _of = require('rxjs/observable/of');

var _filter = require('rxjs/operators/filter');

var _map = require('rxjs/operators/map');

var _withLatestFrom = require('rxjs/operators/withLatestFrom');

var _switchMap = require('rxjs/operators/switchMap');

var _mergeMap = require('rxjs/operators/mergeMap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var epicMiddleware = {};
var combine = _reduxObservable.combineEpics;
var rootEpicWrapperFunc = function rootEpicWrapperFunc(rootEpic) {
    return rootEpic;
};
var store = null;

function getPauseActionType(actionKey) {
    return "@@PauseableEpic/[" + actionKey + "]::PAUSE";
}
function getResumeActionType(actionKey) {
    return "@@PauseableEpic/[" + actionKey + "]::RESUME";
}
function createPauseAction(actionKey) {
    return { type: getPauseActionType(actionKey) };
}
function createResumeAction(actionKey) {
    return { type: getResumeActionType(actionKey) };
}

function wrapAsPausableEpic(epic, key) {
    epic = function () {
        var innerEpic = epic;
        var p_r_ActionKey = key || epic.prototype.constructor.name;
        return function () {
            var pauseActionType = getPauseActionType(p_r_ActionKey);
            var resumeActionType = getResumeActionType(p_r_ActionKey);

            return function (action$, store) {
                var pauser = action$.pipe((0, _filter.filter)(function (act) {
                    return act.type === pauseActionType || act.type === resumeActionType;
                }), (0, _switchMap.switchMap)(function (act) {
                    return act.type === pauseActionType ? (0, _of.of)(true) : (0, _of.of)(false);
                }), (0, _map.map)(function (v) {
                    return v;
                }));

                return action$.pipe((0, _withLatestFrom.withLatestFrom)(pauser), (0, _filter.filter)(function (_ref) {
                    var _ref2 = _slicedToArray(_ref, 2),
                        action = _ref2[0],
                        paused = _ref2[1];

                    return !paused;
                }), (0, _map.map)(function (v) {
                    return v[0];
                }), (0, _filter.filter)(function (act) {
                    return act.type !== pauseActionType && act.type !== resumeActionType;
                }), (0, _mergeMap.mergeMap)(function (action) {
                    return innerEpic(new _reduxObservable.ActionsObservable((0, _of.of)(action)), store);
                }));
            }.apply(this, arguments);
        };
    }();
    return epic;
}

function wrapperFunctionPausableEpic(epic) {
    return wrapAsPausableEpic(epic);
}
function noWrap(epic) {
    return epic;
}
function reducerCombineEpics(combinedEpics) {
    return combine.apply(undefined, _toConsumableArray(Object.values(combinedEpics)));
}
function noReducer(combinedEpics) {
    return combinedEpics;
}

function travelEpicsRecursively(epics, wrapFunction, returningReducerFunction) {
    // If this is a leaf or already combined.
    if (typeof epics === 'function') {
        return wrapFunction(epics);
    }

    // If this is an object of functions, combine epics.
    if ((typeof epics === 'undefined' ? 'undefined' : _typeof(epics)) === 'object') {
        var combinedEpics = {};
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = Object.keys(epics)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var key = _step.value;

                combinedEpics[key] = travelEpicsRecursively(epics[key], wrapFunction, returningReducerFunction);
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        return returningReducerFunction(combinedEpics);
    }

    // If we get here we have an invalid item in the epic path.
    throw new Error({
        message: 'Invalid item in epic tree',
        item: epics
    });
}

function createEpicInjectorMiddleware(initialEpics) {
    var refEpicSubject = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function (input$) {};


    var initialEpics_AsPausables = travelEpicsRecursively(initialEpics, wrapperFunctionPausableEpic, noReducer);

    epicMiddleware = (0, _reduxObservable.createEpicMiddleware)(travelEpicsRecursively(initialEpics_AsPausables, noWrap, reducerCombineEpics), refEpicSubject);

    epicMiddleware.injectedEpics = initialEpics_AsPausables;
    epicMiddleware.runningEpicsMap = {};
    Object.keys(initialEpics).forEach(function (k) {
        epicMiddleware.runningEpicsMap[k] = true;
    });

    var pre_epicMiddleware = function () {
        var _epicMiddleware = epicMiddleware;
        return function (_store) {
            store = _store;
            return _epicMiddleware.apply(this, arguments);
        };
    }();

    return pre_epicMiddleware;
}

function injectEpic(key, epic) {
    var force = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var wait = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

    // If already set, do nothing.
    if (!(0, _has2.default)(epicMiddleware.injectedEpics, key) || force) {
        (0, _set2.default)(epicMiddleware.injectedEpics, key, wrapAsPausableEpic(epic, key));
        (0, _set2.default)(epicMiddleware.runningEpicsMap, key, true);

        if (!wait) {
            epicMiddleware.replaceEpic(travelEpicsRecursively(epicMiddleware.injectedEpics, noWrap, reducerCombineEpics));
        }
    }
    if (!wait) {
        Object.keys(epicMiddleware.injectedEpics).forEach(function (k) {
            if ((0, _has2.default)(epicMiddleware.runningEpicsMap, k) && epicMiddleware.runningEpicsMap[k] === true) {
                resumeEpic(k);
            }
        });
    }
}

function pauseEpic(key) {
    if ((0, _has2.default)(epicMiddleware.injectedEpics, key)) {
        (0, _set2.default)(epicMiddleware.runningEpicsMap, key, false);
        store.dispatch(createPauseAction(key));
    }
}

function resumeEpic(key) {
    if ((0, _has2.default)(epicMiddleware.injectedEpics, key)) {
        (0, _set2.default)(epicMiddleware.runningEpicsMap, key, true);
        store.dispatch(createResumeAction(key));
    }
}